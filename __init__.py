# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Set Origin To Selection",
    "author": "Madman’s Nest",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "3DView Operator",
    "description": "Set mesh origin to current selection",
    "warning": "",
    "category": "Mesh",
}

import bpy, mathutils

class SetOriginToSelection(bpy.types.Operator):
    """Set mesh origin to current selection"""
    bl_idname = "object.set_origin_to_selection"
    bl_label = "Set Origin to Selection"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None and bpy.context.mode == 'EDIT_MESH'

    def execute(self, context):
        stash_3dcursor = mathutils.Vector(bpy.context.scene.cursor.location)
        print(stash_3dcursor)
        bpy.ops.view3d.snap_cursor_to_selected()
        bpy.ops.object.editmode_toggle()
        bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
        bpy.ops.object.editmode_toggle()
        print(stash_3dcursor)
        bpy.context.scene.cursor.location = stash_3dcursor
        print(stash_3dcursor)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(SetOriginToSelection)

def unregister():
    bpy.utils.unregister_class(SetOriginToSelection)

if __name__ == "__main__":
    register()
